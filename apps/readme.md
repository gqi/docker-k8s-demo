## Docker image

https://hub.docker.com/_/nginx

https://github.com/nginxinc/docker-nginx/blob/master/stable/alpine/Dockerfile

1. docker build -t guoyingqi/uniprot-demo:0.01 .
1. docker push guoyingqi/uniprot-demo:0.01


1. docker build -t guoyingqi/uniprot-demo:0.02 .
1. docker push guoyingqi/uniprot-demo:0.02

https://hub.docker.com/repository/docker/guoyingqi/uniprot-demo

1. docker run --name uniprot-simple-nginx -d -p 8080:80 guoyingqi/uniprot-demo:0.01

   http://localhost:8080
   
1. docker run --name uniprot-simple-nginx2 -d -p 9090:80 guoyingqi/uniprot-demo:0.02

   http://localhost:9090/
