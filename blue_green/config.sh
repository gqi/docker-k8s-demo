#!/bin/bash

SERVICE_NAME=$ENV
if [ $ENV == 'test' ]; then
  SERVICE_NAME=${SERVICE_NAME}-${TARGET_ROLE}
fi

sed "s~\$TARGET_ROLE~${TARGET_ROLE}~g" |
  sed "s~\$IMAGE_VERSION~${IMAGE_VERSION}~g" |
  sed "s~\$ENV~${ENV}~g" |
  sed "s~\$SERVICE_NAME~${SERVICE_NAME}~g" |
  sed "s~\$NODE_PORT~${NODE_PORT}~g" |
  tee
