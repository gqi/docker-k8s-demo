#!/bin/bash

export TARGET_ROLE=green
export IMAGE_VERSION=guoyingqi/nginx-green
export ENV=test
export NODE_PORT=30122

# To run deployment:
cat nginx.deployment.yml | sh config.sh | kubectl apply -f -
cat nginx.service.yml | sh config.sh | kubectl apply -f -

# http://193.62.55.83:30122/
